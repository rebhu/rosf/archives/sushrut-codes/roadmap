![sushrut.codes](img/sushrut.codes-logo.png)

# Cure Discovery Task Force - About

## Vision
**We will disrupt medical field. We are building a better strategy. Our projects were too trivial. So, we deleted them.**

## License
[LICENSE.md](LICENSE.md)

## Management
An autonomous open source group at ROSF focussed on making medical science open.

## Made by

![ros.foundation](img/ros.foundation-logo.png)